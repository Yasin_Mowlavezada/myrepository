package com.pomtech.yasin.mycalculator;

import android.view.View;
import android.widget.EditText;

/**
 * Created by M.Yasin on 4/3/2017.
 */

public class ClearOneButton implements View.OnClickListener {
    EditText editText;

    public ClearOneButton(EditText editText) {
        this.editText = editText;
    }

    @Override
    public void onClick(View v) {
        if(String.valueOf(editText.getText()).equalsIgnoreCase("0")
                || String.valueOf(editText.getText()).equalsIgnoreCase("-0") ){

        }else {
            String text = String.valueOf(editText.getText());
            editText.setText(text.substring(0 , text.length() - 1));
        }
    }
}
