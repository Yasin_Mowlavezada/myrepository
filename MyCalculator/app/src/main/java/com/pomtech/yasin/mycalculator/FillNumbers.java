package com.pomtech.yasin.mycalculator;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import static android.content.ContentValues.TAG;

/**
 * Created by M.Yasin on 4/3/2017.
 */

public class FillNumbers implements View.OnClickListener {

    EditText editText;

    public FillNumbers(EditText editText) {
        this.editText = editText;
    }

    @Override
    public void onClick(View v) {

        if((editText.getText() + "").equals("0") ||
                (editText.getText() + "").equals("-0")){
            editText.setText("");
        }

        String text = (editText.getText()+"");

        if (v.getId() == R.id.zero_button){
            if (String.valueOf(editText.getText()).equalsIgnoreCase("0")
                    || String.valueOf(editText.getText()).equalsIgnoreCase("-0")
                    || String.valueOf(editText.getText()).equalsIgnoreCase("-0.0")
                    || String.valueOf(editText.getText()).equalsIgnoreCase("0.0")){

            }else{
                editText.setText(text + "0");
            }
        }else if (v.getId() == R.id.one_button){
            editText.setText(text + "1");
        }else if (v.getId() == R.id.two_button){
            editText.setText(text + "2");
        }else if (v.getId() == R.id.three_button){
            editText.setText(text + "3");
        }else if (v.getId() == R.id.four_button){
            editText.setText(text + "4");
        }else if (v.getId() == R.id.five_button){
            editText.setText(text + "5");
        }else if (v.getId() == R.id.six_button){
            editText.setText(text + "6");
        }else if (v.getId() == R.id.seven_button){
            editText.setText(text + "7");
        }else if (v.getId() == R.id.eight_button){
            editText.setText(text + "8");
        }else if (v.getId() == R.id.nine_button){
            editText.setText(text + "9");
        }

    }
}
