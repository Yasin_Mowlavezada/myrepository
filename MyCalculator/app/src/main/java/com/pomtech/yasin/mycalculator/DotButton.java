package com.pomtech.yasin.mycalculator;

import android.view.View;
import android.widget.EditText;

/**
 * Created by M.Yasin on 4/3/2017.
 */

public class DotButton implements View.OnClickListener {
    EditText editText;

    public DotButton(EditText editText) {
        this.editText = editText;
    }

    @Override
    public void onClick(View v) {
        String text = String.valueOf(editText.getText());
        boolean hasDot = false;

        for(int i = 0 ; i < text.length() ; i++){
            if(text.charAt(i) == '.')
                hasDot = true;
        }

        if(hasDot){

        }else{
            editText.setText(text + ".");
        }

    }
}
