package com.pomtech.yasin.mycalculator;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import static android.content.ContentValues.TAG;

/**
 * Created by M.Yasin on 4/3/2017.
 */

public class FillOperation implements View.OnClickListener {

    EditText editText;
    TextView upper_text;

    public FillOperation(EditText editText , TextView upper_text) {
        this.upper_text = upper_text;
        this.editText = editText;
    }

    @Override
    public void onClick(View v) {
        if(MainActivity.first == 0)
             MainActivity.first = Double.parseDouble(String.valueOf(editText.getText()));

        if(v.getId() == R.id.divide_button) {
            MainActivity.operation = "divide";
            upper_text.setText(MainActivity.first + " /");
            editText.setText("0");
        }
        else if (v.getId() == R.id.times_button) {
            MainActivity.operation = "times";
            upper_text.setText(MainActivity.first + " *");
            editText.setText("0");
        }
        else if (v.getId() == R.id.minuse_button) {
            MainActivity.operation = "minuse";
            upper_text.setText(MainActivity.first + " -");
            editText.setText("0");
        }
        else if(v.getId() == R.id.add_button) {
            MainActivity.operation = "add";
            upper_text.setText(MainActivity.first + " +");
            editText.setText("0");
        }

//        Log.d(TAG, "onClick: " + MainActivity.operation);
    }
}
