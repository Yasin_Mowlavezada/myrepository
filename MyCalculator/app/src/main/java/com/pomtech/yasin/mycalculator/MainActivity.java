package com.pomtech.yasin.mycalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button  clear_all ,sign , percent , divide , times , minuse , add , equals;
    Button dot , zero , one , two , three , four , five , six , seven , eight ,nine ;
    View clear_one;
    EditText editText;
    TextView upper_text;

    static  double first = 0 ;
    static  double second = 0;
    static String operation = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        clear_one = findViewById(R.id.clean_button);
        clear_all = (Button) findViewById(R.id.clear_all_button);
        sign = (Button) findViewById(R.id.sign_button);
        percent = (Button) findViewById(R.id.percent_button);
        divide = (Button) findViewById(R.id.divide_button);
        times = (Button) findViewById(R.id.times_button);
        minuse = (Button) findViewById(R.id.minuse_button);
        add = (Button) findViewById(R.id.add_button);
        equals = (Button) findViewById(R.id.equals_button);

        dot = (Button) findViewById(R.id.dot_button);
        zero = (Button) findViewById(R.id.zero_button);
        one = (Button) findViewById(R.id.one_button);
        two = (Button) findViewById(R.id.two_button);
        three = (Button) findViewById(R.id.three_button);
        four = (Button) findViewById(R.id.four_button);
        five = (Button) findViewById(R.id.five_button);
        six = (Button) findViewById(R.id.six_button);
        seven = (Button) findViewById(R.id.seven_button);
        eight = (Button) findViewById(R.id.eight_button);
        nine = (Button) findViewById(R.id.nine_button);

        editText = (EditText) findViewById(R.id.inputsTextField);
        upper_text = (TextView) findViewById(R.id.upper_text);

        sign.setOnClickListener(new SignButton(editText));
        percent.setOnClickListener(new PercentButton(editText));
        clear_all.setOnClickListener(new ClearAllButton(editText , upper_text));
        clear_one.setOnClickListener(new ClearOneButton(editText));
        dot.setOnClickListener(new DotButton(editText));
        divide.setOnClickListener(new FillOperation(editText , upper_text));
        times.setOnClickListener(new FillOperation(editText , upper_text));
        minuse.setOnClickListener(new FillOperation(editText , upper_text));
        add.setOnClickListener(new FillOperation(editText , upper_text));

        zero.setOnClickListener(new FillNumbers(editText));
        one.setOnClickListener(new FillNumbers(editText));
        two.setOnClickListener(new FillNumbers(editText));
        three.setOnClickListener(new FillNumbers(editText));
        four.setOnClickListener(new FillNumbers(editText));
        five.setOnClickListener(new FillNumbers(editText));
        six.setOnClickListener(new FillNumbers(editText));
        seven.setOnClickListener(new FillNumbers(editText));
        eight.setOnClickListener(new FillNumbers(editText));
        nine.setOnClickListener(new FillNumbers(editText));

        equals.setOnClickListener(new EqualsButton(editText , upper_text));

    }

}
