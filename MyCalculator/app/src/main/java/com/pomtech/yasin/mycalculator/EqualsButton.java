package com.pomtech.yasin.mycalculator;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by M.Yasin on 4/3/2017.
 */

public class EqualsButton implements View.OnClickListener {

    EditText editText;
    TextView upper_text;

    Double result;

    public EqualsButton(EditText editText , TextView upper_text) {
        this.editText = editText;
        this.upper_text = upper_text;
    }


    @Override
    public void onClick(View v) {

        MainActivity.second = Double.parseDouble(editText.getText() + "");
        result = 0.0;

        if(MainActivity.operation.equals("divide")){
            result = MainActivity.first / MainActivity.second;
        }else if(MainActivity.operation.equals("times")){
            result = MainActivity.first * MainActivity.second;
        }else if(MainActivity.operation.equals("minuse")){
            result = MainActivity.first - MainActivity.second;
        }else if(MainActivity.operation.equals("add")){
            result = MainActivity.first + MainActivity.second;
        }

        upper_text.setText(result + "");

        MainActivity.first = result;

    }
}
