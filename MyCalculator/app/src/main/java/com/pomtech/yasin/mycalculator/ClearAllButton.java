package com.pomtech.yasin.mycalculator;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by M.Yasin on 4/3/2017.
 */

public class ClearAllButton implements View.OnClickListener {

    EditText editText;
    TextView upper_text;

    public ClearAllButton(EditText editText , TextView upper_text) {
        this.editText = editText;
        this.upper_text = upper_text;
    }

    @Override
    public void onClick(View v) {
        editText.setText("0");
        MainActivity.operation = "";
        upper_text.setText("0");
        MainActivity.first = 0 ;
        MainActivity.second = 0 ;
    }
}
