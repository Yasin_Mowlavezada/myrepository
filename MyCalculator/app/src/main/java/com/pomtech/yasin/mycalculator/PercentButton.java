package com.pomtech.yasin.mycalculator;

import android.view.View;
import android.widget.EditText;

/**
 * Created by M.Yasin on 4/3/2017.
 */

public class PercentButton implements View.OnClickListener {

    EditText editText ;

    public PercentButton(EditText editText) {
        this.editText = editText;
    }

    @Override
    public void onClick(View v) {

        MainActivity.first = Double.parseDouble(String.valueOf(editText.getText()));
        editText.setText(String.valueOf((MainActivity.first / 100.00)));
    }
}
