package com.pomtech.yasin.mycalculator;

import android.view.View;
import android.widget.EditText;

/**
 * Created by M.Yasin on 4/3/2017.
 */

public class SignButton implements View.OnClickListener {

    EditText editText ;

    public SignButton(EditText editText) {
        this.editText = editText;
    }

    @Override
    public void onClick(View v) {
        String text = String.valueOf(editText.getText());
        if(text.charAt(0) == '-'){
            text = text.substring(1);
            editText.setText(text);
        }else {
            editText.setText("-" + text);
        }
    }
}
